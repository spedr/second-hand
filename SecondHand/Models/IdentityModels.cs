﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using System;

namespace SecondHand.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public DateTime BirthDate { get; set; }
        public byte[] ImageFile { get; set; }
        public string ImageMimeType { get; set; }
        public String FullName { get; set; }
        public int Rating { get; set; }
        public int NumberOfRatings { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<SecondHand.Models.Category> Categories { get; set; }
        public System.Data.Entity.DbSet<SecondHand.Models.Product> Products { get; set; }
        public System.Data.Entity.DbSet<SecondHand.Models.Negotiation> Negotiations { get; set; }

        public System.Data.Entity.DbSet<SecondHand.Models.AdminData> AdminDatas { get; set; }
        //public System.Data.Entity.DbSet<SecondHand.Models.AdminData> AdminData { get; set; }
    }
}