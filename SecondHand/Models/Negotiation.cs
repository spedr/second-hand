﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecondHand.Models
{
    public class Negotiation
    {
        public int id { get; set; }
        public string seller { get; set; }
        public string buyer { get; set; }
        public int productId { get; set; }
        public int control { get; set; }
    }
}