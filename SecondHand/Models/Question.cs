﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecondHand.Models
{
    public class Question
    {
        public string question { get; set; }
        public string reply { get; set;}
        public int id { get; set; }
        public string asker { get; set; }
        
    }
}