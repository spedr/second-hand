﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecondHand.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Product name is required")]
        [MaxLength(45, ErrorMessage = "The maximum length must be upto 45 characters only")]
        public string Name { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Has to be decimal with two decimal points")]
        public Decimal Price { get; set; }

        public byte[] Picture { get; set; }

        public string Description { get; set; }
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public string owner { get; set; }

        [Required]
        [Display(Name = "Sale spot")]
        public string SaleSpot { get; set; }

        //state = 0 OPEN_ANNOUNCEMENT
        //state = 1 IN_NEGOTIATION
        //state = 2 ANNOUNCEMENT_CLOSED
        public int state { get; set; }
        public Negotiation negotiation { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}