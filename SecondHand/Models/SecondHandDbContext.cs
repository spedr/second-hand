﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SecondHand.Models
{
    public class SecondHandDbContext:DbContext
    {
        public SecondHandDbContext() : base("SecondHand")
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

    }

}