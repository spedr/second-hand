﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecondHand.Models
{
    public class AdminData
    {
        public int id { get; set; }
        public int AnnouncedItems { get; set; }
        public int AskedQuestions { get; set; }
        public int SoldProducts { get; set; }
        public int RepliedQuestions { get; set; }
    }
}