﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SecondHand.Models;
using SecondHand.ViewModels;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Web.UI.WebControls;

namespace SecondHand.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        public ActionResult Index()
        {
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user;
            var products = db.Products.Include(p => p.Category);
            foreach(Product p in products)
            {
                user = userManager.FindByIdAsync(p.owner).Result;
                p.owner = user.FullName;
            }
            return View(products.ToList());
        }

        public ActionResult MyProducts()
        {
            var userId = User.Identity.GetUserId();
            var products = (from p in db.Products where p.owner == userId select p).OrderBy(x => x.state);
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user;
            foreach (Product p in products)
            {
                user = userManager.FindByIdAsync(p.owner).Result;
                p.owner = user.FullName;
            }
            return View("Index", products.ToList());
        }

        // GET: Products/Search
        public ActionResult Search(string Query)
        {

            var products = (from p in db.Products select p);
            if (!String.IsNullOrEmpty(Query))
            {
                products = products.Where(s => s.Name.Contains(Query)).OrderBy(x => x.state);
            }
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user;
            foreach (Product p in products)
            {
                user = userManager.FindByIdAsync(p.owner).Result;
                p.owner = user.FullName;
            }
            return View(products.ToList());
        }

        public ActionResult DeleteComment(int productId, int pos, string userId)
        {
            if (userId == User.Identity.GetUserId())
            {
                Product product = db.Products.Find(productId);
                product.Questions.Remove(product.Questions.ElementAt(pos));
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = productId });
            }
            else return HttpNotFound();

        }

        // GET: Products/Reply
        public ActionResult Reply(int productId, string userId, string question, int pos)
        {
            CommentViewModel model = new CommentViewModel { asker = userId, question = question, productId = productId, cont = pos };
            return View(model);
        }

        //POST: Products/Reply
        [HttpPost]
        public ActionResult ReplyPost(int productId, int cont, string reply)
        {
            Product product = db.Products.Find(productId);
            product.Questions.ElementAt(cont).reply = reply;
            var AdminData = db.AdminDatas.FirstOrDefault();
            AdminData.RepliedQuestions++;
            db.Entry(AdminData).State = EntityState.Modified;
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details", new { id = productId });
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByIdAsync(product.owner).Result;
            ViewBag.name = user.FullName;

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public ActionResult UserName2(Product model)
        {
            if (model == null)
            {
                return HttpNotFound();
            }

            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByIdAsync(model.owner).Result;
            ViewBag.name = user.FullName;
            return Content(user.FullName);
        }

        [HttpPost]
        public ActionResult Search(SearchViewModel model)
        {
            var products = from p in db.Products select p;
            if (!String.IsNullOrEmpty(model.Query))
            {
                products = products.Where(s => s.Name.Contains(model.Query));
            }

            return View(products.ToList());
        }

        public ActionResult UserName(Product model)
        {
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByIdAsync(model.owner).Result;
            return Content(user.FullName);
        }

        // GET: Products/Create
        public ActionResult Create()
        {

            var model = new ProductViewModel { Categories = GetCategory() };
            return View(model);
        }

        
        public IEnumerable<SelectListItem> GetCategory()
        {
            var categories = db.Categories.ToList();
            List<SelectListItem> items = new List<SelectListItem>();

            

            foreach (var a in categories)
            {
                items.Add(new SelectListItem { Text = a.Name, Value = ""+a.Id });
            }

            return items;
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel model)
        {
            //var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var product = new Product { Name = model.Name, Description = model.Description, CategoryId = model.SelectedCategoryId, Price = model.Price , SaleSpot = model.SaleSpot};
                product.Questions = new List<Question>();
                product.owner = User.Identity.GetUserId();
                HttpPostedFileBase ImageToUpload = Request.Files[0];

                var Filetype = ImageToUpload.ContentType;
                product.Picture = new Byte[ImageToUpload.ContentLength];
                ImageToUpload.InputStream.Read(product.Picture, 0, ImageToUpload.ContentLength);
                db.Products.Add(product);
                var AdminData = db.AdminDatas.FirstOrDefault();
                AdminData.AnnouncedItems++;
                db.Entry(AdminData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            model.Categories = GetCategory();

            return View(model);
        }

        public ActionResult Rating(String id)
        {  
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(id);
            if(user.NumberOfRatings > 5)
            {
                if(user.Rating < 5)  { return Content("not very trusted"); }
                if(user.Rating < 10) { return Content("caution when buying from this seller"); }
                if(user.Rating > 10) { return Content("trusted"); }
                if(user.Rating > 20) { return Content("very trusted"); }
                if(user.Rating > 30) { return Content("top seller"); }
            }
            return Content("not yet rated");
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);

            if(User.Identity.GetUserId() != product.owner)
            {
                return HttpNotFound();
            }
            if (product == null)
            {
                return HttpNotFound();
            }
            var model = new ProductViewModel { Id = product.Id, Name = product.Name, Description = product.Description, Price = product.Price, SelectedCategoryId = product.CategoryId, Picture = product.Picture, Categories = GetCategory(), SaleSpot = product.SaleSpot };
            return View(model);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel model)
        {
            //var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var product = new Product { Id = model.Id, Name = model.Name, Description = model.Description, CategoryId = model.SelectedCategoryId, Price = model.Price, owner = User.Identity.GetUserId() , SaleSpot = model.SaleSpot, state = 0};
                HttpPostedFileBase ImageToUpload = Request.Files[0];

                var Filetype = ImageToUpload.ContentType;
                product.Picture = new Byte[ImageToUpload.ContentLength];
                ImageToUpload.InputStream.Read(product.Picture, 0, ImageToUpload.ContentLength);
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            model.Categories = GetCategory();

            return View(model);
        }


        [HttpPost]
        public ActionResult Ask(int productId, string userId, string comment)
        {

            Product product = db.Products.Find(productId);
            Question question = new Models.Question { asker = userId, question = comment };
            product.Questions.Add(question);
            db.Entry(product).State = EntityState.Modified;
            var AdminData = db.AdminDatas.FirstOrDefault();
            AdminData.AskedQuestions++;
            db.Entry(AdminData).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Details", new { id = productId });
        }

        public ActionResult ProductName(int id)
        {
            if (id == null)
            {
                return Content("no name specified");
            }
            var product = db.Products.Find(id);
            return Content(product.Name);

        }



        [Authorize]
        public ActionResult InitiateNegotiation(string buyer, string seller, int productId)
        {
            Product product = db.Products.Find(productId);
            if (product.state == 0)
            {
                product.negotiation = new Negotiation { buyer = buyer, seller = seller, productId = productId, control = 0 };
                product.state = 1;
                db.Negotiations.Add(product.negotiation);
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                var negs = from p in db.Negotiations where p.productId == productId select p.id;

                return RedirectToAction("Details", "Negotiations", new { id = negs.First() });
            }
            return HttpNotFound();
        }

        // GET: Products/Delete/5
        [Authorize(Users = "admin@s2b.br")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@s2b.br")]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
