﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SecondHand.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace SecondHand.Controllers
{
    public class NegotiationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Negotiations
        [Authorize]
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var test = (from a in db.Negotiations where (a.buyer == userId || a.seller == userId) select a).ToList();

            return View(test);
        }

        // GET: Negotiations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Negotiation negotiation = db.Negotiations.Find(id);
            if (negotiation == null)
            {
                return HttpNotFound();
            }
            return View(negotiation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TerminateNegotiation(int id)
        {
            Negotiation negotiation = db.Negotiations.Find(id);
            Product product = db.Products.Find(negotiation.productId);
            product.state = 0;
            product.negotiation = null;
            db.Negotiations.Remove(negotiation);
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NegotiationSuccessful(int id)
        {
            Negotiation negotiation = db.Negotiations.Find(id);
            Product product = db.Products.Find(negotiation.productId);
            if (negotiation.control == 0)
            {
                negotiation.control++;
                //product.negotiation.control++;
                db.Entry(product).State = EntityState.Modified;
                db.Entry(negotiation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            if(negotiation.control == 1)
            {
                negotiation.control++;
                //product.negotiation.control++;
                product.state = 2;
                var AdminData = db.AdminDatas.FirstOrDefault();
                AdminData.SoldProducts++;
                db.Entry(AdminData).State = EntityState.Modified;
                db.Entry(product).State = EntityState.Modified;
                db.Entry(negotiation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Rating", new { id = negotiation.seller });
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Negotiations/Rating
        public ActionResult Rating(string id)
        {
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindById(id);
            var MyList = new List<int> { 0, 1, 2, 3, 4 , 5};
            SelectList items2 = new SelectList(MyList);

            ViewBag.MyList = items2;

            return View(user);
        }

        //POST: Negotiations/Rating/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RatingPost(string id, int rating)
        {

            ApplicationUser user = db.Users.Find(id);

            if (rating == 0) user.Rating -= 3;
            if (rating == 1) user.Rating -= 2;
            if (rating == 3) user.Rating += 2;
            if (rating == 4) user.Rating += 4;
            if (rating == 5) user.Rating += 5;
            user.NumberOfRatings++;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        // GET: Negotiations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Negotiation negotiation = db.Negotiations.Find(id);
            if (negotiation == null)
            {
                return HttpNotFound();
            }
            return View(negotiation);
        }

        // POST: Negotiations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Negotiation negotiation = db.Negotiations.Find(id);
            Product product = db.Products.Find(negotiation.productId);
            product.negotiation = null;
            product.state = 0;
            db.Entry(product).State = EntityState.Modified;
            db.Negotiations.Remove(negotiation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
