﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SecondHand.Models;

namespace SecondHand.Controllers
{
    [Authorize(Users = "admin@s2b.br")]
    public class AdminDatasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        
        // GET: AdminDatas/Index/5
        public ActionResult Index(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdminData adminData = db.AdminDatas.Find(id);
            if (adminData == null)
            {
                adminData = new AdminData { AnnouncedItems = 0, AskedQuestions = 0, SoldProducts = 0  };
                db.AdminDatas.Add(adminData);
                db.SaveChanges();
            }
            return View(adminData);
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
