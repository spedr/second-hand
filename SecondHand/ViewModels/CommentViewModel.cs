﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecondHand.ViewModels
{
    public class CommentViewModel
    {
        public string question { get; set; }
        public string reply { get; set; }
        public int cont { get; set; }
        public string asker { get; set; }
        public int productId { get; set; }
    }
}