﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SecondHand.Startup))]
namespace SecondHand
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
