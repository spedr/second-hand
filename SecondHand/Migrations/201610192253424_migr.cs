namespace SecondHand.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Negotiations", "productId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Negotiations", "productId");
        }
    }
}
