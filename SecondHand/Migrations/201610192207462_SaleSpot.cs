namespace SecondHand.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SaleSpot : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "SaleSpot", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "SaleSpot");
        }
    }
}
