namespace SecondHand.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Negotiations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Negotiations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        seller = c.String(),
                        buyer = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Products", "negotiation_id", c => c.Int());
            CreateIndex("dbo.Products", "negotiation_id");
            AddForeignKey("dbo.Products", "negotiation_id", "dbo.Negotiations", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "negotiation_id", "dbo.Negotiations");
            DropIndex("dbo.Products", new[] { "negotiation_id" });
            DropColumn("dbo.Products", "negotiation_id");
            DropTable("dbo.Negotiations");
        }
    }
}
