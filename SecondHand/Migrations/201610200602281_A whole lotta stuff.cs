namespace SecondHand.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Awholelottastuff : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminDatas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        AnnouncedItems = c.Int(nullable: false),
                        AskedQuestions = c.Int(nullable: false),
                        SoldProducts = c.Int(nullable: false),
                        RepliedQuestions = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdminDatas");
        }
    }
}
