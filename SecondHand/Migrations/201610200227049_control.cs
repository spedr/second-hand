namespace SecondHand.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class control : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Negotiations", "control", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Negotiations", "control");
        }
    }
}
